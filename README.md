# EGNR-Hawarden by Zero Dollar Payware

**[Offical File Post](https://forum.inibuilds.com/files/file/1655-egnr-hawarden/)**

**[Offical EGNR Forum](https://forum.inibuilds.com/forum/91-zero-dollar-payware-hawarden-egnr/)**

**[Official ZDP Discord](https://discord.gg/aArWgU5)**


**Required Libraries**
* MisterX Library
* RA Library
* SAM


**Installation Instructions:**

Download and move "egnr-hawarden-1.x.x" (and if you are installing the optional ortho, also "yEGNR-Hawarden-Overlay" and "zEGNR-Hawarden-Ortho") folder(s) into your custom scenery folder

Use xOrganizer or delete your scenery_packs.ini to adjust your scenery load order. If you have issues with the scenery loading most likely your load order is wrong or you are missing the RA library. 


**How to use the Beluga Line Station (BLS)**
1. Upon landing, taxi to either the the beluga apron (diamond shaped area) or holding point 1 just in front of the BLS. 
2. Show down your engines
3. Open the SAM menu and under the "controls" page, open both sets of BLS doors
4. Call Better Pushback and direct it to tow you into the building. Note: there are alignment markers where your main gear goes in the red striped area outside the BLS. 
5. Get towed into the BLS and disconnect the tow. 
6. Close the BLS doors. You only need to close the ST doors, but you can close both if you want since the fuselage of the ST is smaller than the XL. 

To leave the BLS, perform the same steps in reverse. Command better pushback to put you on the beluga apron for startup. 

[Parking Locations](https://i.imgur.com/aWu20dH.jpg)

[Better Pushback Position](https://i.imgur.com/T4THIJf.png)

[Better Pushback Reference Markers](https://i.imgur.com/CVdh3o6.jpg)

**Scenery Versions**
* Standard - This is the most realistc version and is the default version installed
* Standard Flattened - This is the same as standard, except the terrain is flattened. This is helpful if you are not using ortho with inaccurate mesh
* Online Network - This version includes the fictional victor ramp. This ramp adds 10 parking positions for the Beluga and is useful if you are flying on VATSIM/IVAO/etc. with high traffic. The real airport only has enough capcity for 1 beluga arrival every 30 minutes, this apron allows for much more aircraft movements. 
* Online Network Flattened - This is the same as the Online Network version, but with the airport terrain flattened for use without ortho or if your mesh is bad. 


**How to change scenery versions**

Note: The standard version comes installed by default

1. Make sure xplane is closed
2. Go to the "Scenery Variants" folder and open the version you want to install
3. Copy the "Earth nav data" folder into the main EGNR-Hawarden folder. Select "yes" when prompted to overwrite the existing files. 
4. Launch xplane


**Citations/Credits**

World Imagery (orthophotos)

Esri. "Imagery" [basemap]. Scale Not Given. "World Imagery". January 21, 2021. https://www.arcgis.com/home/item.html?id=10df2279f9684e4a9f6a7f08febac2a9. (February 18, 2021).

MisterX Library

RA Library

SAM Library

iniBuilds

ZDP Developers: StableSystem, awsfiu, TJ

Please report any bugs on the Zero Dollar Payware discord or on the official forum thread (linked above)

To conctact us about this repo please send a message on [the org](https://forums.x-plane.org/index.php?/profile/534962-stablesystem/)

This repository and it's contents are protected under [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)
Assets used from other developers was done so with their knowledge and approval. 

