A
800
FOREST

TEXTURE undergrowth.png
NO_SHADOW

SCALE_X 2048				
SCALE_Y 2048

SPACING 1.0 1.0
RANDOM 5 5

#	low-left	tex size	center	percent	--height--
# tree	s	t	w	y	offset	occur	min	max	quads	type	name
#------------------------------------------------------------------------------------------
TREE	1401	1547	647		501		325		20		1.0 	2.0 	2 	1 	bush3
TREE	824		0		1224	859		603		10		3.0 	5.0 	2 	1 	bush4
TREE	1561	1058	974 	976		437		20		0.5 	1.5 	2 	1 	bush6

SKIP_SURFACE water
